const express = require('express');
const router = express.Router();

const fileController = require('./fileController.js')

router.get('/',fileController.getFiles);

router.get(`/:filename`,fileController.getFile);

router.post('/',fileController.createFile);

router.delete(`/:filename`, fileController.deleteFile);

router.patch(`/:filename`, fileController.patchFile);

module.exports = router;