const fs = require('fs');
const promisifiedFS = require("fs/promises");

module.exports.createFile = async function createFile (req, res) { 
  const format = ['log', 'txt', 'json', 'yaml', 'xml'];
  const {filename,content} = req.body;

  let formatData = '';
  format.forEach((elem) => {
    let resalt = filename.match(`${elem}$`);
    if(resalt){
      formatData = resalt[0];
    }
  });

  if(!formatData) {
    return res.status(400).json({"message": `This file type not supported`});
  }

  if(!filename || !content){
    return res.status(400).json({"message": "Please specify 'content' or 'filename' parameters"});
  }
  
  let files = JSON.parse( await promisifiedFS.readFile("./api/elements.json", "utf8"));

  if(!files.find(elem => elem.filename === filename)) {
    files.push({
      'filename':filename,
      'content' : content,
      'extension': formatData,
    });
  } else {
    return res.status(400).json({"message": "A file with the same name already exists"});
  }
  
  fs.writeFile(`api/elements.json`, JSON.stringify(files), (err) => {
    if(err) {
      return res.status(500).json({"message": "Server error"});
    }
  });

  fs.writeFile(`api/files/${filename}`, content, (err) => {
    if(err) {
      return res.status(500).json({"message": "Server error"});
    }
    return res.status(200).json({"message": "File created successfully"});
  }); 
};

module.exports.getFile = async function getFile (req, res) {

  const files = JSON.parse(await promisifiedFS.readFile("./api/elements.json", "utf8"));
  let name = req.params.filename;
  let file = files.find(elem => elem.filename == name);

  if(!file) {
    return res.status(400).json({"message": `No file with ${name} filename found`});
  } 
  fs.stat(`api/files${req.path}`,(err, stats) => {
    if(err) {
      return res.status(500).json({"message": "Server error"});
    }

    return res.status(200).json({
      "message": "Success",
      "filename": name,
      "content": file.content,
      "extension": file.extension,
      "uploadedDate": stats.birthtime
    });
  });
};

module.exports.getFiles = async function getFiles (req, res) {
  let files = JSON.parse(await promisifiedFS.readFile("./api/elements.json", "utf8"));
  res.json({
    "message": "Success",
    "files": files.map((elem) => elem.filename)
  });
};

module.exports.deleteFile = async function deleteFile (req, res) {
  let files = JSON.parse(await promisifiedFS.readFile("./api/elements.json", "utf8"));
  const index = files.findIndex(elem => elem.filename == req.params.filename);

  if (index !== -1) {
    files.splice(index, 1);
  }else{
    return res.status(400).json({"message": "No such file"});
  }

  fs.writeFile(`api/elements.json`, JSON.stringify(files), (err) => {
    if(err) {
      return res.status(500).json({"message": "Server error"});
    }
  });

  fs.unlink(`api/files${req.path}`, (err) => {
    if (err) {
      return res.status(500).json({"message": "Server error"});
    }
    return res.status(200).json({"message": "File delete successfully"});
  });
};

module.exports.patchFile = async function patchFile (req, res) {
  if(!req.body.content){
    return res.status(400).json({"message": "Please specify 'content' parameter"});
  }

  let files = JSON.parse(await promisifiedFS.readFile("./api/elements.json", "utf8"));
  const index = files.findIndex(elem => elem.filename == req.params.filename);

  if (index !== -1) {
    files[index].content = req.body.content;
  }else{
    return res.status(400).json({"message": "No such file"});
  }

  fs.writeFile(`api/files${req.path}`,req.body.content , (err) => {
    if(err) {
      return res.status(500).json({"message": "Server error"});
    }
  });

  fs.writeFile(`api/elements.json`, JSON.stringify(files), (err) => {
    if(err) {
      return res.status(500).json({"message": "Server error"});
    }
  });

  return res.status(200).json({"message": "File modify successfully"});
};



