const express = require('express');
const morgan = require('morgan');
const app = express();

const fileRouter = require('./fileRouter.js');

app.use(morgan('combined'));
app.use(express.json());

app.use(`/api/files`,fileRouter);

app.listen(8080);